using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvancedGameManager : MonoBehaviour
{
    public static AvancedGameManager instance;

    public EstadoJuego estado;
    public EstadoJugador estadoJugador;


    public Carta Mazo, Decision;


    private void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);

        Iniciar();

    }


    void Iniciar()
    {
        estado = EstadoJuego.Pregunta;
        estadoJugador = EstadoJugador.JUGANDO;
        Mazo.BocaArriba = false;
    }


    public void Decidir()
    {
        estado = EstadoJuego.Vista;

        SacarCarta();
    }

    void SacarCarta()
    {
        float random = Random.Range(0, 100);
        if (random > 75)
        {
            Mazo.Valor = ValorCarta.AZUL;
        }else if (random > 50 && random < 75)
        {
            Mazo.Valor = ValorCarta.ROJO;
        }
        if (random < 50 && random > 25)
        {
            Mazo.Valor = ValorCarta.VERDE;
        }
        else if(random < 25)
        {
            Mazo.Valor = ValorCarta.AMARILLO;
        }
        CompararCartas();
    }


    void CompararCartas()
    {
        if (Decision.Valor == Mazo.Valor)
            estadoJugador = EstadoJugador.GANASTE;
        else
            estadoJugador = EstadoJugador.PERDISTE;
    }
}
