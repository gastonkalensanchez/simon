using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UIElements;

public class CardOutPutController : MonoBehaviour
{
    public Carta CartasSO;

    public SpriteRenderer spriteRenderer;


    private void Start()
    {
        
        CartasSO.OnDarVuelta += OnDarVuelta;
    }



    void OnDarVuelta()
    {
        if (!CartasSO.BocaArriba)
        {
            spriteRenderer.color = Color.black;
        }
        else
        {
            if (CartasSO.Valor == ValorCarta.ROJO)
            {
                spriteRenderer.color = Color.red;
            }
            else if (CartasSO.Valor == ValorCarta.AZUL)
            {
                spriteRenderer.color = Color.blue;
            }
            else if (CartasSO.Valor == ValorCarta.VERDE)
            {
                spriteRenderer.color = Color.green;
            }
            else if (CartasSO.Valor == ValorCarta.AMARILLO)
            {
                spriteRenderer.color = Color.yellow;
            }
        }


    }
}
