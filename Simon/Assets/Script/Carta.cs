using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card/Card ViewModel" , fileName = "CardViewModel")]
public class Carta : ScriptableObject
{
    public ValorCarta Valor;

    public Action OnDarVuelta;

    public bool BocaArriba
    {
        get
        {
            return _bocaArriba;
        }

        set
        {
            _bocaArriba = value;
            OnDarVuelta?.Invoke();
        }
    }


    [SerializeField] bool _bocaArriba;


    private void Awake()
    {
        BocaArriba = new bool();
    }
}
