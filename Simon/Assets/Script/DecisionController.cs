using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DecisionController : MonoBehaviour
{
   
    public ValorCarta ValorCartaJugador;
    void Start()
    {
        ValorCartaJugador = ValorCarta.NADA;      
    }
    
    public void Amarrillo()
    {
     
        ValorCartaJugador = ValorCarta.AMARILLO;

    }

    public void Verde()
    {      
      ValorCartaJugador = ValorCarta.VERDE;
    }

    public void Azul()
    {
  
        ValorCartaJugador = ValorCarta.AZUL;


    }

    public void Rojo()
    {
        ValorCartaJugador = ValorCarta.ROJO;
    }
    
}
