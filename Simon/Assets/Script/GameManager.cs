using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum EstadoJuego { Pregunta, Vista, Final }
public enum EstadoJugador { GANASTE, PERDISTE, JUGANDO }
public enum ValorCarta { NADA, ROJO, AZUL, VERDE, AMARILLO }
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public DecisionController DecisionTomada;

    public CardOutPutController PropiedadesCarta;

    public EstadoJuego estado;
    public EstadoJugador estadoJugador;

    public ValorCarta mazoDeCartas;
    public ValorCarta decision;

    public TMP_Text texto;




    bool Click = false;
    public float sacarcartatemp = 0;
    public float darvueltatemp = 0;



    private void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);

        estado = EstadoJuego.Vista;
        estadoJugador = EstadoJugador.JUGANDO;
        PropiedadesCarta.CartasSO.BocaArriba = false;
    }
    void Update()
    {
        if (estado == EstadoJuego.Pregunta)
        {
            Decidir();
        }
        else if (estado == EstadoJuego.Vista)
        {
            SacarCarta();
        }


    }


    void Decidir()
    {
        darvueltatemp += Time.deltaTime;
        if (darvueltatemp < 5)
        {
            if (Click)
            {
                Click = false;
                decision = DecisionTomada.ValorCartaJugador;
                CompararCartas();
            }

        }
        else
        {
            CompararCartas();

        }


    }

    void SacarCarta()
    {
        sacarcartatemp += Time.deltaTime;
        if (sacarcartatemp > 5)
        {
            float random = Random.Range(0, 100);
            if (random > 75)
            {
                mazoDeCartas = ValorCarta.AZUL;
                estado = EstadoJuego.Pregunta;
                PropiedadesCarta.CartasSO.Valor = ValorCarta.AZUL;
                PropiedadesCarta.CartasSO.BocaArriba = true;
                 StartCoroutine("Cambiar");
            }
            else if (random > 50 && random < 75)
            {
                mazoDeCartas = ValorCarta.ROJO;
                estado = EstadoJuego.Pregunta;
                PropiedadesCarta.CartasSO.Valor = ValorCarta.ROJO;
                PropiedadesCarta.CartasSO.BocaArriba = true;
                StartCoroutine("Cambiar");

            }
            if (random < 50 && random > 25)
            {
                mazoDeCartas = ValorCarta.VERDE;
                estado = EstadoJuego.Pregunta;
                PropiedadesCarta.CartasSO.Valor = ValorCarta.VERDE;
                PropiedadesCarta.CartasSO.BocaArriba = true;
                 StartCoroutine("Cambiar");
                
            }
            else if (random < 25)
            {
                mazoDeCartas = ValorCarta.AMARILLO;
                estado = EstadoJuego.Pregunta;
                PropiedadesCarta.CartasSO.Valor = ValorCarta.AMARILLO;
                PropiedadesCarta.CartasSO.BocaArriba = true;
                StartCoroutine("Cambiar");


            }
            sacarcartatemp = 0;

           
        }

    }


    void CompararCartas()
    {
        if (decision == mazoDeCartas)
        {
            estadoJugador = EstadoJugador.JUGANDO;
            estado = EstadoJuego.Vista;
            darvueltatemp = 0;
            PropiedadesCarta.CartasSO.BocaArriba = false;
            texto.text = "SEGUI ASI";

        }
        else
        {   
            estadoJugador = EstadoJugador.PERDISTE;
            estado = EstadoJuego.Final;
            darvueltatemp = 0;

            PropiedadesCarta.CartasSO.BocaArriba = false;
            texto.text = "Perdiste";

        }
            
    }

    public void keloke(bool pClick)
    {

        Click = pClick;
    }

    IEnumerator Cambiar()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        PropiedadesCarta.CartasSO.BocaArriba = false;

    }












}
